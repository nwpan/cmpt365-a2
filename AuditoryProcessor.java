import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.JFileChooser;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FFmpegFrameGrabber;

public class AuditoryProcessor {

	public static final String TITLE = "Single Frame Render";
	private static boolean   DEBUG = true; //set to true to enable debug output
	private static CanvasFrame canvas;
	private static JFileChooser fileChooser = new JFileChooser();

	@SuppressWarnings("unused")
	public static FFmpegFrameGrabber getVideoGrabber()
	{
		if (fileChooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
			return null;
		}

		// Load the image
		final String path = fileChooser.getSelectedFile().getAbsolutePath();
		final FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(path);
		if (grabber != null) {
			return grabber;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unused")
	public static BufferedImage getBufferedImage()
	{
		if (fileChooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
			return null;
		}

		// Load the image
		final String path = fileChooser.getSelectedFile().getAbsolutePath();
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Color pixelColor;
		Color pixelColorGray;
		BufferedImage newImage = image;
		int height = newImage.getHeight(), width = newImage.getWidth();
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				pixelColor = new Color(newImage.getRGB(x, y));
				int red = (int)(0.299*pixelColor.getRed());
				int green = (int)(0.587*pixelColor.getGreen());
				int blue = (int)(0.114*pixelColor.getBlue());
				pixelColorGray = new Color((red+green+blue),(red+green+blue),(red+green+blue));
				newImage.setRGB(x, y, pixelColorGray.getRGB());
			}
		}

		if (newImage != null) {
			return newImage;
		} else {
			return null;
		}         
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Class.forName("org.bytedeco.javacpp.swresample");
		canvas = new CanvasFrame("Auditory Multimedia Processor");
		canvas.setCanvasSize(800, 600);
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

		MenuItem fOpenVideo = new MenuItem();
		fOpenVideo.setLabel("Open Video File");
		MenuItem fOpenImage = new MenuItem();
		fOpenImage.setLabel("Open Image File");
		MenuItem fClose = new MenuItem();
		fClose.setLabel("Exit");


		Menu mFile = new Menu();
		mFile.setLabel("File");
		mFile.add(fOpenVideo);
		mFile.add(fOpenImage);
		mFile.add(fClose);

		MenuBar menuBar = new MenuBar();
		menuBar.add(mFile);

		canvas.setMenuBar(menuBar);

		fOpenVideo.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				byte[] abData;
				float fSampleRate = 8000.0F;
				float fSignalFrequency = 1000.0F;
				float fAmplitude = 0.7F;
				AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, fSampleRate, 16, 2, 4, fSampleRate, false);
				AudioInputStream  oscillator;
				SourceDataLine  line = null;
				DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);

				try
				{
					line = (SourceDataLine) AudioSystem.getLine(info);
					line.open(audioFormat);
				}
				catch (LineUnavailableException e)
				{
					e.printStackTrace();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				BufferedImage img; 
				double[][] data;
				FFmpegFrameGrabber grabber = AuditoryProcessor.getVideoGrabber();
				try
				{
					grabber.start();
					IplImage frameImg;
					line.start();
					if (DEBUG) { System.out.println("=== VIDEO PROCESSING STARTED ==="); }
					Color pixelColor;
					Color pixelColorGray;
					while ((frameImg = grabber.grab()) != null)
					{
						canvas.setCanvasSize(grabber.getImageWidth(), grabber.getImageHeight());

						if (frameImg != null)
						{
							//canvas.showImage(frameImg);
							img = (BufferedImage) frameImg.getBufferedImage();
							
							BufferedImage newImage = img;
							int height = newImage.getHeight(), width = newImage.getWidth();
							for (int y = 0; y < height; y++)
							{
								for (int x = 0; x < width; x++)
								{
									pixelColor = new Color(newImage.getRGB(x, y));
									int red = (int)(0.299*pixelColor.getRed());
									int green = (int)(0.587*pixelColor.getGreen());
									int blue = (int)(0.114*pixelColor.getBlue());
									pixelColorGray = new Color((red+green+blue),(red+green+blue),(red+green+blue));
									newImage.setRGB(x, y, pixelColorGray.getRGB());
								}
							}
							
							img = newImage;
							canvas.showImage(img);
							data = new double[img.getHeight()][img.getWidth()];
							for (int y = 0; y < img.getHeight(); y++)
							{
								for (int x = 0; x < img.getWidth(); x++)
								{
									double img2double = ((double)((img.getRGB(x,y) >> 8) & 0xff)+1)/255;
									double fiximg = (double)((int)(img2double*15))/15;
									data[y][x] = fiximg;
								}
							}

							oscillator = new Oscillator(
									fSignalFrequency,
									fAmplitude,
									audioFormat,
									AudioSystem.NOT_SPECIFIED,
									data,
									img.getHeight(),
									img.getWidth()
									);

							abData = new byte[(int) Math.round(audioFormat.getFrameRate()/img.getHeight()) * audioFormat.getFrameSize() * img.getWidth()];
							if (DEBUG) { System.out.println("Data trying to read (bytes): " + abData.length); }
							int nRead = oscillator.read(abData, 0, abData.length);
							if (DEBUG) { System.out.println("Data in loop, read (bytes): " + nRead); }
							int nWritten = line.write(abData, 0, nRead);
							if (DEBUG) { System.out.println("Data written: " + nWritten); }
						}
					}
					if (DEBUG) { System.out.println("=== VIDEO PROCESSING FINISHED ==="); }
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
		
		fOpenImage.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				byte[] abData;
				float fSampleRate = 8000.0F;
				float fSignalFrequency = 1000.0F;
				float fAmplitude = 0.7F;
				AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, fSampleRate, 16, 2, 4, fSampleRate, false);
				AudioInputStream  oscillator;
				SourceDataLine  line = null;
				DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);

				try
				{
					line = (SourceDataLine) AudioSystem.getLine(info);
					line.open(audioFormat);
				}
				catch (LineUnavailableException err)
				{
					err.printStackTrace();
				}
				catch (Exception err)
				{
					err.printStackTrace();
				}
				double[][] data;
				BufferedImage img = AuditoryProcessor.getBufferedImage();
				try
				{
					line.start();
					canvas.setCanvasSize(img.getWidth(), img.getHeight());

					if (img != null)
					{
						canvas.showImage(img);
						data = new double[img.getHeight()][img.getWidth()];
						if (DEBUG) { System.out.println("=== IMAGE PROCESSING STARTED ==="); }
						for (int y = 0; y < img.getHeight(); y++)
						{
							for (int x = 0; x < img.getWidth(); x++)
							{
								double img2double = ((double)((img.getRGB(x,y) >> 8) & 0xff)+1)/255;
								double fiximg = (double)((int)(img2double*15))/15;
								data[y][x] = fiximg;
							}
						}

						oscillator = new Oscillator(
								fSignalFrequency,
								fAmplitude,
								audioFormat,
								AudioSystem.NOT_SPECIFIED,
								data,
								img.getHeight(),
								img.getWidth()
								);
						
						abData = new byte[(int) Math.round(audioFormat.getFrameRate()/img.getHeight()) * audioFormat.getFrameSize() * img.getWidth()];
						if (DEBUG) { System.out.println("Data trying to read (bytes): " + abData.length); }
						int nRead = oscillator.read(abData, 0, abData.length);
						if (DEBUG) { System.out.println("Data in loop, read (bytes): " + nRead); }
						int nWritten = line.write(abData, 0, nRead);
						if (DEBUG) { System.out.println("Data written: " + nWritten); }
						if (DEBUG) { System.out.println("=== IMAGE PROCESSING FINISHED ==="); }
					}
				}
				catch (Exception err)
				{
					err.printStackTrace();
				}
			}
		});
		
		fClose.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
			
		});
	}
}