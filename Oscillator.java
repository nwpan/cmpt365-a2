import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class Oscillator
	extends AudioInputStream
{
	private static final boolean	DEBUG = false;

	private byte[]		m_abData;
	private int			m_nBufferPosition;
	private long		m_lRemainingFrames;
	
	public Oscillator(
			  float fSignalFrequency,
			  float fAmplitude,
			  AudioFormat audioFormat,
			  long lLength,
			  double[][] data,
			  int rowLength,
			  int colLength)
	{
		super(new ByteArrayInputStream(new byte[0]),
		      new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
				      audioFormat.getSampleRate(),
				      16,
				      2,
				      4,
				      audioFormat.getFrameRate(),
				      audioFormat.isBigEndian()),
		      lLength);
		if (DEBUG) { out("Oscillator.<init>(): begin"); }
		m_lRemainingFrames = lLength;
		fAmplitude = (float) (fAmplitude * Math.pow(2, getFormat().getSampleSizeInBits() - 1));
		int nPeriodLengthInFrames = Math.round(getFormat().getFrameRate()/rowLength);
		int nBufferLength = nPeriodLengthInFrames * getFormat().getFrameSize();
		
		m_abData = new byte[nBufferLength*colLength*nPeriodLengthInFrames];
		double[] freq = initFreq(rowLength, 440.0);
		int[] signal;
		
		float[] ss;
		float[] tt = new float[nPeriodLengthInFrames];
		
		for (int nFrame = 1; nFrame <= nPeriodLengthInFrames; nFrame++)
		{
			tt[nFrame-1] = (float) nFrame / audioFormat.getSampleRate();
		}
		int currBase = 0;
		for (int col = 0; col < colLength; col++)
		{
			signal = new int[nPeriodLengthInFrames];
			ss = new float[nPeriodLengthInFrames];
			for (int nFrame = 1; nFrame <= nPeriodLengthInFrames; nFrame++)
			{
				int m = 0;
				for (int row = 0; row < rowLength; row++)
				{
					m = (rowLength-1)-row;
					ss[nFrame-1] = (float) Math.sin(2.0 * Math.PI * tt[nFrame-1] * freq[m]);
					signal[nFrame-1] = signal[nFrame-1] + ((int) Math.round(data[row][col] * ss[nFrame-1] * fAmplitude));
				}
				signal[nFrame-1] = signal[nFrame-1]/rowLength;
				int nBaseAddr = currBase + (nFrame) * getFormat().getFrameSize();
				m_abData[nBaseAddr + 0] = (byte) (signal[nFrame-1] & 0xFF);
				m_abData[nBaseAddr + 1] = (byte) ((signal[nFrame-1] >>> 8) & 0xFF);
				m_abData[nBaseAddr + 2] = (byte) (signal[nFrame-1] & 0xFF);
				m_abData[nBaseAddr + 3] = (byte) ((signal[nFrame-1] >>> 8) & 0xFF);
			}
			currBase += nPeriodLengthInFrames * getFormat().getFrameSize();
		}
	}


	public int available()
	{
		int	nAvailable = 0;
		if (m_lRemainingFrames == AudioSystem.NOT_SPECIFIED)
		{
			nAvailable = Integer.MAX_VALUE;
		}
		else
		{
			long	lBytesAvailable = m_lRemainingFrames * getFormat().getFrameSize();
			nAvailable = (int) Math.min(lBytesAvailable, (long) Integer.MAX_VALUE);
		}
		return nAvailable;
	}

	public int read()
		throws IOException
	{
		if (DEBUG) { out("Oscillator.read(): begin"); }
		throw new IOException("cannot use this method currently");
	}



	public int read(byte[] abData, int nOffset, int nLength)
		throws IOException
	{
		if (DEBUG) { out("Oscillator.read(): begin"); }
		if (nLength % getFormat().getFrameSize() != 0)
		{
			throw new IOException("length must be an integer multiple of frame size");
		}
		int	nConstrainedLength = Math.min(available(), nLength);
		int	nRemainingLength = nConstrainedLength;
		while (nRemainingLength > 0)
		{
			int	nNumBytesToCopyNow = m_abData.length - m_nBufferPosition;
			nNumBytesToCopyNow = Math.min(nNumBytesToCopyNow, nRemainingLength);
			System.arraycopy(m_abData, m_nBufferPosition, abData, nOffset, nNumBytesToCopyNow);
			nRemainingLength -= nNumBytesToCopyNow;
			nOffset += nNumBytesToCopyNow;
			m_nBufferPosition = (m_nBufferPosition + nNumBytesToCopyNow) % m_abData.length;
		}
		int	nFramesRead = nConstrainedLength / getFormat().getFrameSize();
		if (m_lRemainingFrames != AudioSystem.NOT_SPECIFIED)
		{
			m_lRemainingFrames -= nFramesRead;
		}
		int	nReturn = nConstrainedLength;
		if (m_lRemainingFrames == 0)
		{
			nReturn = -1;
		}
		if (DEBUG) { out("Oscillator.read(): end"); }
		return nReturn;
	}

	private double[] initFreq(int length, double centreFreq)
	{
        double[] freq = new double[length];
        int mid = length/2-1;
        freq[mid] = centreFreq;
        for (int m = mid+1; m < length; m++)
        {
        	freq[m] = freq[m-1]*Math.pow(2, (1.0/12.0));
        }
        for (int m = mid-1; m >= 0; m--)
        {
        	freq[m] = freq[m+1]*Math.pow(2, (-1.0/12.0));
        }
        return freq;
	}	
	
	private static void out(String strMessage)
	{
		System.out.println(strMessage);
	}
}
